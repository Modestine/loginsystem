
<?php 
require_once("includes/header.php");
?>
<div class="jumbotron">
  <h1 class="display-4">Oops, We Couldn't Find This Page</h1>
  <p class="lead">This happens because you have type a wrong url or the link is broken.</p>
  <hr class="my-4">
  <p>Our Team is Working On it at the moment to find out what went wrong!</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="index.php" role="button">Go Back To Home Page</a>
  </p>
</div>