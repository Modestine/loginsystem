
<?php 
require_once("includes/headersigninup.php");
require_once("includes/classes/Account.php"); 
require_once("includes/classes/Constants.php"); 
require_once("includes/classes/FormSanitizer.php"); 


$account = new Account($con);

if(isset($_POST["submitButton"])){

  $email = FormSanitizer::sanitizeFormEmail($_POST["email"]);
  $password = FormSanitizer::sanitizeFormPassword($_POST["password"]);

  $result = $account->login($email, $password);
  if($result){
    
      $_SESSION["userLoggedIn"] = $email;
      header("Location: index.php");
  }

};

function getInputValue($name){
  if(isset($_POST[$name])){
    echo $_POST[$name];
  }
}
?>


    <form class="form-signin" action="signin.php" method="POST">
      <div class="text-center mb-1">
        <img class="" src="img/1.png" alt="Logo" width="100" height="100">
        <h1 class="h5 mb-3 font-weight-normal">Modestine Web Services</h1>
        <p class="font-weight-light">Sign in to continue.</p>
      </div>

      <div class="form-group">
      <?php echo $account->getError(Constants::$loginFailed); ?>
            <label for="email" class="font-weight-light">Email Address</label>
            <input type="email" class="form-control" id="email" name="email" value="<?php getInputValue('email')?>" placeholder="Enter Email" required>
      </div>

      <div class="form-group">
            <label for="password" class="font-weight-light">Password</label>
            <input type="password" class="form-control" id="password" name="password" autocomplete="off" placeholder="Enter Password" required>
      </div>

      <!-- <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div> -->
      <input type="submit" name="submitButton" id="submitButton" class="btn btn-lg btn-primary btn-block btn-dark" value="Sign In">
      <div class="mt-3">
            <a href="signup.php" class="font-weight-normal">Don't Have An Account? Sign Up Here!</a>
            </div>
      <!-- <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button> -->
      <p class="mt-5 mb-3 text-muted text-center">&copy; 2018</p>
    </form>

    <script src="plugins/jQuery/jQuery.min.js"></script>
  <script src="plugins/bootstrap/bootstrap.min.js"></script>
   
  <script>
    
  if('serviceWorker' in navigator){
    navigator.serviceWorker
        .register('sw.js')
        .then(function(){
            console.log('Service worker registered');
        });
}
  </script>
  </body>
</html>