<?php
class Constants {
    public static $firstNameCharacters = "Your first name must be between 2 and 25 characters! ";
    public static $lastNameCharacters = "Your last name must be between 2 and 25 characters! ";
    public static $emailDoNotMatch = "Your emails don't match!";
    public static $emailInvalid = "Please enter a valid email address!";
    public static $emailUsed = "This email address is already used!";
    public static $passwordDoNotMatch = "Passwords don't match!";
    public static $passwordNotValid = "You password can only contain letters and numbers!";
    public static $passwordLength = "You password must be between 5 and 30 characters!";
    public static $loginFailed = "wrong email or password. Try again!";




}


?>