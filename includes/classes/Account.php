<?php

class Account {

    private $con;
    private $errorArray = array();

    public function __construct($con){
        $this->con = $con;
    }

    public function login($em, $pw){

        $salt = "b84ebbc83b0f167c467784a5362942b6";
        $pw1 = hash("sha512", $pw); 
        $pw = md5($salt.$pw1);
        
        $query = $this->con->prepare("SELECT * FROM users WHERE email=:em AND password=:pw");
        $query->bindParam(":em", $em);
        $query->bindParam(":pw", $pw);

        $query->execute();

        if($query->rowCount() == 1) {
            return true;
        }else {
            array_push($this->errorArray, Constants::$loginFailed);
            return false;
        }


    }


    public function register($fn, $ln, $em, $em2, $pw, $pw2){
        $this->validateFirstName($fn);
        $this->validateLastName($ln);
        $this->validateEmails($em, $em2);
        $this->validatePasswords($pw, $pw2);

        if(empty($this->errorArray)){      
            return $this->insertUserDetails($fn, $ln, $em, $pw);
        }else{
            return false;
        }

    }

    public function insertUserDetails($fn, $ln, $em, $pw){
      
        $salt = "b84ebbc83b0f167c467784a5362942b6";
        $pw1 = hash("sha512", $pw); 
        $pw = md5($salt . $pw1); 
        $profilePicture = "img/default.png";

        $query = $this->con->prepare("INSERT INTO users (`firstName`,`lastName`,`email`,`password`,`profilePic`)
                                    VALUES(:fn, :ln, :em, :pw, :pic)");
        $query->bindParam(":fn", $fn);
        $query->bindParam(":ln", $ln);
        $query->bindParam(":em", $em);
        $query->bindParam(":pw", $pw);
        $query->bindParam(":pic", $profilePicture);

        return $query->execute();

    }

    private function validateFirstName($fn) {
        if(strlen($fn) > 25 || strlen($fn) < 2) {
            array_push($this->errorArray, Constants::$firstNameCharacters);
        }
    }

    private function validateLastName($ln) {
        if(strlen($ln) > 25 || strlen($ln) < 2) {
            array_push($this->errorArray, Constants::$lastNameCharacters);
            return;
        }
    }

    private function validateEmails($em, $em2) {
        if($em != $em2) {
            array_push($this->errorArray, Constants::$emailDoNotMatch);
        }

        if(!filter_var($em, FILTER_VALIDATE_EMAIL)){
            array_push($this->errorArray, Constants::$emailInvalid);
            return;
        }

        $query = $this->con->prepare("SELECT email FROM users WHERE email=:em");
        $query->bindParam(":em", $em);
        $query->execute();

        if($query->rowCount() != 0){
            array_push($this->errorArray, Constants::$emailUsed);
        }
    }
    
    private function validatePasswords($pw, $pw2) {
        if($pw != $pw2) {
            array_push($this->errorArray, Constants::$passwordDoNotMatch);
            return;
        }

        if (preg_match("/[^A-Za-z0-9]/", $pw)) {
            array_push($this->errorArray, Constants::$passwordNotValid);
            return;
        }

        if(strlen($pw) > 25 || strlen($pw) < 2) {
            array_push($this->errorArray, Constants::$passwordLength);
            // return;
        }
    }

    public function getError($error) {
        if(in_array($error, $this->errorArray)) {
            return "<p class='text-danger'>$error</p>";
        }
    }
}

?>