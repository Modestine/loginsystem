<?php require_once("includes/header.php"); ?>
<?php
if(!isset($_SESSION["userLoggedIn"])){
    header("Location: signin.php");
}

?>

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <!-- <button class="btn btn-primary" id="menu-toggle">Toggle Menu</button> -->
        <div class="d-flex w-100  justify-content-between">
        <img class="pr-5" src="img/menu.png" id="menu-toggle" alt="Menu">
        
        <div class="navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item pr-2">
            <div class="d-flex  justify-content-between">
            <input class="form-control form-control-sm mr-5" name="search" id="search" type="text" placeholder="Search">
            <img class="" src="img/default.png" id="menu-toggle" alt="Profile" width="30" height="30">
            </div>
            </li> 
            <!-- <li class="nav-item">
            <img class="" src="img/default.png" id="menu-toggle" alt="Profile" width="30" height="30">
            </li>  -->

          </ul>
       
        </div>
        </div>

      </nav>
  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading bg-dark text-light"><?php echo $_SESSION['userLoggedIn'];?> </div>
      <div class="list-group list-group-flush">
        <a href="#" class="list-group-item list-group-item-action bg-light">Dashboard</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Shortcuts</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Overview</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Events</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Profile</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Status</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    

    <!-- Page Content -->
    <div id="page-content-wrapper">



      <div class="container-fluid">
        <h1 class="mt-4">Simple Sidebar</h1>
        <p>The starting state of the menu will appear collapsed on smaller screens, and will appear non-collapsed on larger screens. When toggled using the button below, the menu will change.</p>
        <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>. The top navbar is optional, and just for demonstration. Just create an element with the <code>#menu-toggle</code> ID which will toggle the menu when clicked.</p>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>


  <?php require_once("includes/footer.php"); ?>

</body>

</html>
