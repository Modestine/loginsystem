<?php 
require_once("includes/config.php");
require_once("includes/headersigninup.php");
require_once("includes/classes/Account.php"); 
require_once("includes/classes/Constants.php"); 
require_once("includes/classes/FormSanitizer.php"); 

$account = new Account($con);

if(isset($_POST["submitButton"])){
  $firstName = FormSanitizer::sanitizeFormString($_POST["firstName"]);
  $lastName = FormSanitizer::sanitizeFormString($_POST["lastName"]);

  $email = FormSanitizer::sanitizeFormEmail($_POST["email"]);
  $email2 = FormSanitizer::sanitizeFormEmail($_POST["email2"]);

  $password = FormSanitizer::sanitizeFormPassword($_POST["password"]);
  $password2 = FormSanitizer::sanitizeFormPassword($_POST["password2"]);

  $result = $account->register($firstName, $lastName, $email, $email2, $password, $password2);

  if($result){
    
      $_SESSION["userLoggedIn"] = $email;
      header("Location: index.php");
  }

};

function getInputValue($name){
  if(isset($_POST[$name])){
    echo $_POST[$name];
  }
}

?>

    <form class="form-signup" action="signup.php" method="POST">
      <div class="text-center">
        <img class="" src="img/1.png" alt="Logo" width="100" height="100">
        <h1 class="h5 mb-1 font-weight-normal">Modestine Web Services</h1>
        <p class="font-weight-light">Sign up to get started.</p>
      </div>

  <div class="form-row">
    <div class="col-sm-6">
    <label for="firstName" class="font-weight-light">First Name *</label>
                <?php echo $account->getError(Constants::$firstNameCharacters); ?>
    <input type="text" class="form-control" id="firstName" name="firstName" value="<?php getInputValue('firstName')?>" placeholder="First Name" required>

    </div>
    <div class="col-sm-6">
    <label for="lastName" class="font-weight-light">Last Name *</label>
                <?php echo $account->getError(Constants::$lastNameCharacters); ?>
    <input type="text" class="form-control" id="lastName" name="lastName" value="<?php getInputValue('lastName')?>" placeholder="Last Name" required>
    </div>
  </div>

  <div class="form-row">
    <div class="col-sm-6">
    <label for="email" class="font-weight-light">Email Address *</label>
                <?php echo $account->getError(Constants::$emailDoNotMatch); ?>
                <?php echo $account->getError(Constants::$emailInvalid); ?>
                <?php echo $account->getError(Constants::$emailUsed); ?>
    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="<?php getInputValue('email')?>" placeholder="Enter email" required>
                <small id="emailHelp" class="form-text text-muted">Enter a valid email address.</small>
    </div>
    <div class="col-sm-6">
    <label for="email2" class="font-weight-light">Confirm Email *</label>
    <input type="email" class="form-control" id="email2" name="email2" aria-describedby="emailHelp2" value="<?php getInputValue('email2')?>" placeholder="Enter email" required>
                <small id="emailHelp2" class="form-text text-muted">Please confirm the email address.</small>
    </div>
  </div>

  <div class="form-row mb-3">
    <div class="col-sm-6">
    <label for="password" class="font-weight-light">Password *</label>
                <?php echo $account->getError(Constants::$passwordDoNotMatch); ?>
                <?php echo $account->getError(Constants::$passwordNotValid); ?>
                <?php echo $account->getError(Constants::$passwordLength); ?>
    <input type="password" class="form-control" id="password" name="password" aria-describedby="passwordlHelp" autocomplete="off" placeholder="Password" required>
                <!-- <small id="passwordlHelp" class="form-text text-muted">Your password must contain at least a pecial caracter</small> -->

    </div>
    <div class="col-sm-6">
    <label for="password2" class="font-weight-light">Confirm Password *</label>

    <input type="password" class="form-control" id="password2" name="password2" placeholder="Password" autocomplete="off" required>
   
    </div>
  </div>

      <input type="submit" name="submitButton" id="submitButton" class="btn btn-lg btn-primary btn-block btn-dark" value="Sign Up">
      
      <p ><small class="text-muted">By clicking Sign Up, you agree to our Terms and Data Policy.</small></p>
      <div class="mt-3">
      <a href="signin.php" class="font-weight-normal">Already Have An Account? Sign In Here!</a>
            </div>
      <!-- <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button> -->
      <!-- <p class="mt-5 mb-3 text-muted text-center">&copy; 2018</p> -->
    </form>


 
   
<script src="plugins/jQuery/jQuery.min.js"></script>
  <script src="plugins/bootstrap/bootstrap.min.js"></script>
   
  <script>
    
  if('serviceWorker' in navigator){
    navigator.serviceWorker
        .register('sw.js')
        .then(function(){
            console.log('Service worker registered');
        });
}
  </script>

</body>
</html>