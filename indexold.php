<?php require_once("includes/headersigninup.php"); ?>


<div class="jumbotron">
  <h1 class="display-4">Hello, <?php

if(isset($_SESSION["userLoggedIn"])){

    echo "<p class='h1'>" . $_SESSION['userLoggedIn'] . " is logged In.</p>";

}else {

  header("Location: signin.php");

}

?>
  
  
  
  
  </h1>
  <p class="lead">This means you have successfully configured your signup - signin system.</p>
  <hr class="my-4">
  <p>This system uses PWA,PHP, POO, .HTACCESS, PDO technologies and the bootstrap framework!</p>
  <a class="btn btn-primary btn-lg" href="https://getbootstrap.com/docs/4.3/getting-started/introduction/" role="button">Learn more</a>
</div>

 <?php require_once("includes/footer.php"); ?>

    <script src="plugins/jQuery/jQuery.min.js"></script>
  <script src="plugins/bootstrap/bootstrap.min.js"></script>
   
  <script>
    
  if('serviceWorker' in navigator){
    navigator.serviceWorker
        .register('sw.js')
        .then(function(){
            console.log('Service worker registered');
        });
}
  </script>
 
  </body>
</html>

                